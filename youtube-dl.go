package youtubedl

import (
	"bufio"
	"encoding/json"
	"errors"
	"github.com/sirupsen/logrus"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
)

func execute(args []string) (string, error) {
	_, closeChan, err := executeProgress(args)
	if err != nil {
		return "", err
	}
	result := <- closeChan
	if result.Err != nil {
		return "", result.Err
	}
	return result.Path, nil
}

func executeExtractor(args []string) (string, error) {
	binary, err := getBinary()
	if err != nil {
		return "", err
	}
	cmd := exec.Command(binary, args...)
	out, err := cmd.StdoutPipe()
	if err != nil {
		return "", err
	}
	if err := cmd.Start(); err != nil {
		return "", err
	}
	scan := bufio.NewScanner(out)
	extractorRegex := regexp.MustCompile(`^(\S.*)$`)
	last := ""
	match := ""
	for scan.Scan() {
		if match == "" || match == "generic" {
			text := scan.Text()
			if res := extractorRegex.FindStringSubmatch(text); res != nil {
				last = res[1]
			} else {
				match = last
			}
		}
	}
	if last == "" {
		return "", errors.New("no extractor was returned")
	}
	return match, nil
}

func executeInfo(args []string) (Info, error) {
	binary, err := getBinary()
	if err != nil {
		return Info{}, err
	}
	cmd := exec.Command(binary, args...)
	output, err := cmd.Output()
	logrus.Debug("Command output: \n" + string(output))
	if err != nil {
		return Info{}, err
	}
	info := Info{}
	if err := json.Unmarshal(output, &info); err != nil {
		return Info{}, err
	}
	return info, nil
}

func executeProgress(args []string) (chan float64, chan Result, error) {
	binary, err := getBinary()
	if err != nil {
		return nil, nil, err
	}
	progressArg := []string{"--newline"}
	args = append(progressArg, args...)
	cmd := exec.Command(binary, args...)
	readOut, err := cmd.StdoutPipe()
	if err != nil {
		return nil, nil, err
	}
	readErr, err := cmd.StderrPipe()
	if err != nil {
		return nil, nil, err
	}
	outScan := bufio.NewScanner(readOut)
	errScan := bufio.NewScanner(readErr)
	defaultPathRegex := regexp.MustCompile(`^\[download] Destination: (.+)$`)
	existingPathRegex := regexp.MustCompile(`^\[download] (.*) has already been downloaded( and merged)?$`)
	ffmpegPathRegex := regexp.MustCompile(`^\[.*] Merging formats into "(.+)"$`)
	multiStageRegex := regexp.MustCompile(`^\[download] Destination: .+\.f\d+\.?.*$`)
	progressRegex := regexp.MustCompile(`(\d+\.\d+)%`)
	eventChan := make(chan float64)
	closeChan := make(chan Result)
	currentStage := -1.
	stages := 1.
	lastError := ""
	var path string
	go func() {
		for outScan.Scan() {
			text := outScan.Text()
			logrus.Debug(text)
			if strings.HasPrefix(text, "[download]") {
				if res := multiStageRegex.FindStringSubmatch(text); res != nil {
					stages = 2
					currentStage++
				} else if res := defaultPathRegex.FindStringSubmatch(text); res != nil {
					path = res[1]
					currentStage++
				} else if res := existingPathRegex.FindStringSubmatch(text); res != nil {
					path = res[1]
				} else if res := progressRegex.FindStringSubmatch(text); res != nil {
					progress, err := strconv.ParseFloat(res[1], 64)
					if err == nil {
						select {
						case eventChan <- progress / stages + 50 * currentStage:
						default:
						}
					}
				}
			} else if res := ffmpegPathRegex.FindStringSubmatch(text); res != nil {
				path = res[1]
			}
		}
		close(eventChan)
		if err := cmd.Wait(); err != nil {
			closeChan <- Result{
				Err: errors.New(lastError),
				Path: "",
			}
		} else {
			closeChan <- Result{
				Err: nil,
				Path: path,
			}
		}
		close(closeChan)
	}()
	go func() {
		for errScan.Scan() {
			lastError += errScan.Text() + "\n"
			logrus.Debug(lastError)
		}
	}()
	if err := cmd.Start(); err != nil {
		return nil, nil, err
	}
	return eventChan, closeChan, nil
}
