package youtubedl

// Represents format templates that can be used for Downloader output paths.
const (
	// Represents the ID of the video.
	ID = "%(id)s"
)