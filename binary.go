package youtubedl

import (
	"errors"
	"os/exec"
)

var binaries = [...]string{"yt-dlp", "youtube-dl"}

func getBinary() (string, error) {
	for _, binary := range binaries {
		if _, err := exec.LookPath(binary); err == nil {
			return binary, nil
		}
	}
	return "", errors.New("youtube-dl binary not found")
}
