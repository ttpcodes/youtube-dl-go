package youtubedl

// Represents the result of a Downloader when called with RunProgress.
type Result struct {
	// Represents an error if any was encountered.
	Err error
	// Represents the output path of the Downloader if successful.
	Path string
}