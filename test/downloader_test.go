package main

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ttpcodes/youtube-dl-go"
	"io/ioutil"
	"os"
	"strings"
	"testing"
)

const (
	DOWNLOAD_PATH = "data/RealOrFake"
	DOWNLOAD_PATH_EXISTING = "data/VirtualDream"
	DOWNLOAD_PATH_NO_FFMPEG = "data/SkyCity"
)

func TestDownload(t *testing.T) {
	// Test that a download of a properly formatted link works.
	t.Run("DownloadSuccess", func(t *testing.T) {
		// Guess what my favorite song is right now?
		downloader := youtubedl.NewDownloader("https://www.youtube.com/watch?v=Qu_OzBsgRcI")
		downloader.Output(DOWNLOAD_PATH)
		path, err := downloader.Run()
		if err != nil {
			t.Error(err)
		}
		if path == "" {
			t.Error("Downloader ran successfully but no path was returned.")
		}
		if _, err := os.Stat(path); os.IsNotExist(err) {
			t.Errorf("Downloader ran successfully but file %s does not exist.", path)
		}
		if !strings.Contains(path, DOWNLOAD_PATH) {
			t.Error("Downloader ran successfully but downloaded to the wrong path.")
		}
		if err := os.Remove(path); err != nil {
			t.Error(err)
		}
	})

	// Test that a download on an existing file works.
	t.Run("DownloadSuccessExisting", func(t *testing.T) {
		// Realize a Virtual Dream, ole!
		downloader := youtubedl.NewDownloader("https://www.youtube.com/watch?v=X7XsTYn-euY")
		downloader.Output(DOWNLOAD_PATH_EXISTING)
		path, err := downloader.Run()
		if err != nil {
			t.Error(err)
		}
		if path == "" {
			t.Error("Downloader ran successfully but no path was returned.")
		}
		if _, err := os.Stat(path); os.IsNotExist(err) {
			t.Errorf("Downloader ran successfully but file %s does not exist.", path)
		}
		if !strings.Contains(path, DOWNLOAD_PATH_EXISTING) {
			t.Error("Downloader ran successfully but downloaded to the wrong path.")
		}

		// Attempt the download again.
		downloader = youtubedl.NewDownloader("https://www.youtube.com/watch?v=X7XsTYn-euY")
		downloader.Output(DOWNLOAD_PATH_EXISTING)
		if err != nil {
			t.Error(err)
		}
		if path == "" {
			t.Error("Downloader ran successfully but no path was returned.")
		}
		if _, err := os.Stat(path); os.IsNotExist(err) {
			t.Errorf("Downloader ran successfully but file %s does not exist.", path)
		}
		if !strings.Contains(path, DOWNLOAD_PATH_EXISTING) {
			t.Error("Downloader ran successfully but downloaded to the wrong path.")
		}
		if err := os.Remove(path); err != nil {
			t.Error(err)
		}
	})

	// Test that a download not involving FFMPEG works.
	t.Run("DownloadSuccessNoFFMPEG", func(t *testing.T) {
		// This was one of my favorite songs back during high school. I recently got reminded of it again.
		downloader := youtubedl.NewDownloader("https://soundcloud.com/xeuphoric/sky-city")
		downloader.Output(DOWNLOAD_PATH_NO_FFMPEG)
		path, err := downloader.Run()
		if err != nil {
			t.Error(err)
		}
		if path == "" {
			t.Error("Downloader ran successfully but no path was returned.")
		}
		if _, err := os.Stat(path); os.IsNotExist(err) {
			t.Errorf("Downloader ran successfully but file %s does not exist.", path)
		}
		if !strings.Contains(path, DOWNLOAD_PATH_NO_FFMPEG) {
			t.Error("Downloader ran successfully but downloaded to the wrong path.")
		}
		if err := os.Remove(path); err != nil {
			t.Error(err)
		}
	})

	// Test that a download of an improperly formatted link errors.
	t.Run("DownloadFailure", func(t *testing.T) {
		downloader := youtubedl.NewDownloader("https://www.youtube.com/watch?v=___________")
		path, err := downloader.Run()
		if err == nil {
			t.Error("Expected error but none was returned.")
		}
		if path != "" {
			t.Error("Downloader failed but some output was still returned.")
		}
	})

	// Test that an asynchronous download of a properly formatted link works.
	t.Run("DownloadProgressSuccess", func(t *testing.T) {
		downloader := youtubedl.NewDownloader("https://www.youtube.com/watch?v=Qu_OzBsgRcI")
		downloader.Output(DOWNLOAD_PATH)
		progressChan, resultChan, err := downloader.RunProgress()
		if err != nil {
			t.Error(err)
		}
		currentProgress := 0.
		for newProgress := range progressChan {
			if newProgress < currentProgress {
				t.Error("Progress of download not incrementing properly.")
			}
			currentProgress = newProgress
		}
		result := <- resultChan
		if currentProgress != 100 {
			t.Error("Download completed but progress is not 100.")
		}
		if result.Err != nil {
			t.Error(result.Err)
		}
		if err := os.Remove(result.Path); err != nil {
			t.Error(err)
		}
	})

	// Test that an asynchronous download of a properly formatted link not involving FFMPEG works.
	t.Run("DownloadProgressSuccessNoFFMPEG", func(t *testing.T) {
		downloader := youtubedl.NewDownloader("https://soundcloud.com/xeuphoric/sky-city")
		downloader.Output(DOWNLOAD_PATH_NO_FFMPEG)
		progressChan, resultChan, err := downloader.RunProgress()
		if err != nil {
			t.Error(err)
		}
		currentProgress := 0.
		for newProgress := range progressChan {
			if newProgress < currentProgress {
				t.Error("Progress of download not incrementing properly.")
			}
			currentProgress = newProgress
		}
		result := <- resultChan
		if currentProgress != 100 {
			t.Error("Download completed but progress is not 100.")
		}
		if result.Err != nil {
			t.Error(result.Err)
		}
		if err := os.Remove(result.Path); err != nil {
			t.Error(err)
		}
	})

	// Test that an asynchronous download with progress ignored works.
	t.Run("DownloadProgressIgnore", func(t *testing.T) {
		downloader := youtubedl.NewDownloader("https://www.youtube.com/watch?v=Qu_OzBsgRcI")
		downloader.Output(DOWNLOAD_PATH)
		_, resultChan, err := downloader.RunProgress()
		if err != nil {
			t.Error(err)
		}
		result := <- resultChan
		if result.Err != nil {
			t.Error(result.Err)
		}
		if err := os.Remove(result.Path); err != nil {
			t.Error(err)
		}
	})

	// Test that an asynchronous download of an improperly formatted link errors.
	t.Run("DownloadProgressFailure", func(t *testing.T) {
		downloader := youtubedl.NewDownloader("https://www.youtube.com/watch?v=___________")
		_, resultChan, err := downloader.RunProgress()
		if err != nil {
			t.Error(err)
		}
		result := <- resultChan
		if result.Err == nil {
			t.Error("Expected error but none was returned.")
		}
	})

	// Test that a URL with a matching extractor returns the correct extractor.
	t.Run("ExtractorMatching", func(t *testing.T) {
		// c-clays's old songs are some nostalgic bangers
		downloader := youtubedl.NewDownloader("https://www.youtube.com/watch?v=5IAtdx0IZwA")
		extractor, err := downloader.GetExtractor()
		if err != nil {
			t.Error(err)
		}
		assert.Equal(t, "youtube", extractor)
	})

	// Test that a URL without a matching extractor returns the generic extractor.
	t.Run("ExtractorUnknown", func(t *testing.T) {
		// stop posting about among us! i'm tired of seeing it!
		downloader := youtubedl.NewDownloader("sussy_baka")
		extractor, err := downloader.GetExtractor()
		if err != nil {
			t.Error(err)
		}
		assert.Equal(t, "generic", extractor)
	})

	// Test that the info of a properly formatted link can be retrieved.
	t.Run("InfoSuccess", func(t *testing.T) {
		file, err := ioutil.ReadFile("data/realorfake.json")
		if err != nil {
			t.Error(err)
		}
		expected := youtubedl.Info{}
		if err := json.Unmarshal(file, &expected); err != nil {
			t.Error(err)
		}
		downloader := youtubedl.NewDownloader("https://www.youtube.com/watch?v=Qu_OzBsgRcI")
		info, err := downloader.GetInfo()
		if err != nil {
			t.Error(err)
		}
		assert.Equal(t, expected, info)
	})

	// Test that no info is retrieved from an improperly formatted link.
	t.Run("InfoFailure", func(t *testing.T) {
		downloader := youtubedl.NewDownloader("https://www.youtube.com/watch?v=___________")
		_, err := downloader.GetInfo()
		if err == nil {
			t.Error("Expected error but none was returned.")
		}
	})
}
